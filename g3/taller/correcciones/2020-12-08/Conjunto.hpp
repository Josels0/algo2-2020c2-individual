
template <class T>
Conjunto<T>::Conjunto() : _raiz(nullptr) {}

template <class T>
Conjunto<T>::~Conjunto() {
    while(this->_raiz != nullptr)
        remover(this->_raiz->valor);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* pos = this->_raiz;
    while(pos != nullptr && pos->valor != clave){
        if(clave < pos->valor)
            pos = pos->izq;
        else
            pos = pos->der;
    }
    return !(pos == nullptr);
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(this->_raiz == nullptr) {
        this->_raiz = new Nodo(clave);
    }
    else {
        Nodo* pos = this->_raiz;
        while (!pertenece(clave)) {
            if (clave < pos->valor) {
                if (pos->izq == nullptr)
                    pos->izq = new Nodo(clave);
                else
                    pos = pos->izq;
            } else {
                if (pos->der == nullptr)
                    pos->der = new Nodo(clave);
                else
                    pos = pos->der;
            }
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    Nodo** padre = &(this->_raiz);
    while((*padre)->valor != clave){
        if(clave < (*padre)->valor)
            padre = &(*padre)->izq;
        else
            padre = &(*padre)->der;
    }
    if((*padre)->der != nullptr){
        const T sig = siguiente((*padre)->valor);
        remover(sig);
        (*padre)->valor = sig;
    }
    else{
        Nodo* aEliminar = *padre;
        if((*padre)->izq != nullptr)
            *padre = (*padre)->izq;
        else
            *padre = nullptr;
        delete(aEliminar);
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) const {
    Nodo* pos = this->_raiz;
    Nodo* res;
    while(pos != nullptr){
        if(clave >= pos->valor)
            pos = pos->der;
        else {
            res = pos;
            pos = pos->izq;
        }
    }
    return res->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* pos = this->_raiz;
    while(pos->izq != nullptr)
        pos = pos->izq;
    return pos->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* pos = this->_raiz;
    while(pos->der != nullptr)
        pos = pos->der;
    return pos->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    if(this->_raiz != nullptr) {
        T val = minimo();
        uint res = 0;
        while (val != maximo()) {
            res++;
            val = siguiente(val);
        }
        res++;
        return res;
    }
    return 0;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}
