template <typename T>
string_map<T>::string_map() : _size(0), _raiz(nullptr){}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() {*this = aCopiar;} // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    if(!empty())
        delete(this);
    for(string c : d._claves)
        this->insert(make_pair(c, d.at(c)));
}

template <typename T>
string_map<T>::~string_map(){
    const set<string> claves = this->_claves;
    for(string c : claves)
        erase(c);
}

template <typename T>
void string_map<T>::insert(const pair<string, T>& aDefinir) {
    if (this->_raiz == nullptr) {
        this->_raiz = new Nodo();
    }
    Nodo *actual = this->_raiz;
    const bool pertenece = count(aDefinir.first) == 1;
    for (int i = 0; i < aDefinir.first.size(); i++) {
        if(!pertenece)
            actual->peaje++;
        if (actual->siguientes[(int) aDefinir.first[i]] != nullptr) {
            actual = actual->siguientes[(int) aDefinir.first[i]];
        }
        else {
            actual->siguientes[(int) aDefinir.first[i]] = new Nodo();
            actual = actual->siguientes[(int) aDefinir.first[i]];
        }
    }
    if(actual->definicion == nullptr) {
        actual->peaje++;
        actual->definicion = new T(aDefinir.second);
        this->_claves.insert(aDefinir.first);
        this->_size++;
    }
    else
        *actual->definicion = aDefinir.second;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    if(count(clave) == 0){
        T def;
        insert(make_pair(clave, def));
    }
    return at(clave);
}

template <typename T>
int string_map<T>::count(const string& clave) const{
    if(this->_raiz != nullptr) {
        Nodo *actual = this->_raiz;
        for (int i = 0; i < clave.size(); i++) {
            if (actual->siguientes[(int)clave[i]] != nullptr)
                actual = actual->siguientes[(int)clave[i]];
            else
                return 0;
        }
        if(actual->definicion != nullptr)
            return 1;
    }
    return 0;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo *actual = this->_raiz;
    for (int i = 0; i < clave.size(); i++)
        actual = actual->siguientes[(int)clave[i]];
    return *actual->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo *actual = this->_raiz;
    for (int i = 0; i < clave.size(); i++)
        actual = actual->siguientes[(int)clave[i]];
    return *actual->definicion;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo *actual = this->_raiz;
    for (int i = 0; i < clave.size(); i++) {
        actual->peaje--;
        Nodo* siguiente = actual->siguientes[(int) clave[i]];
        if(actual->siguientes[(int) clave[i]]->peaje == 1)
            actual->siguientes[(int) clave[i]] = nullptr;
        if(actual->peaje == 0){
            if(actual == this->_raiz)
                this->_raiz = nullptr;
            delete(actual);
        }
        actual = siguiente;
    }
    delete(actual->definicion);
    actual->definicion = nullptr;
    actual->peaje--;
    if(actual->peaje == 0)
        delete (actual);
    this->_size--;
    this->_claves.erase(clave);
}

template <typename T>
int string_map<T>::size() const{
    return this->_size;
}

template <typename T>
bool string_map<T>::empty() const{
    return this->_raiz == nullptr;
}