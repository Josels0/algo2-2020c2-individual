#include <iostream>

using namespace std;

using uint = unsigned int;

// Ejercicio 1

class Rectangulo {
    public:
        Rectangulo(uint alto, uint ancho);
        uint alto();
        uint ancho();
        float area();

    private:
        int alto_;
        int ancho_;

};

Rectangulo::Rectangulo(uint alto, uint ancho) : alto_(alto),ancho_(ancho)/* Completar */ {
};

uint Rectangulo::alto() {
    // Completar
    return alto_;
}

uint Rectangulo::ancho(){
    return ancho_;
}

float Rectangulo::area(){
    return alto_ * ancho_;
}

// Ejercicio 2

// Clase Elipse

class Elipse {
public:
    Elipse(uint a, uint b);
    uint r_a();
    uint r_b();
    float area();
private:
    int a_;
    int b_;
};

Elipse :: Elipse(uint a, uint b) : a_(a),b_(b){
};

uint Elipse::r_a(){
    return a_;
}

uint Elipse::r_b(){
    return b_;
}

float Elipse::area(){
    return 3.14 * a_ * b_;
}
// Ejercicio 3

class Cuadrado {
    public:
        Cuadrado(uint lado);
        uint lado();
        float area();

    private:
        Rectangulo r_;
};

Cuadrado::Cuadrado(uint lado): r_(lado, lado) {}

uint Cuadrado::lado() {
    return r_.alto();
    // Completar
}

float Cuadrado::area() {
    return r_.alto() * r_.ancho();
    // Completar
}

// Ejercicio 4

// Clase Circulo

class Circulo{
public:
    Circulo(uint radio);
    uint radio();
    float area();
private:
    Elipse e_;
};

Circulo :: Circulo(uint radio) : e_(radio,radio){
};

uint Circulo :: radio(){
    return e_.r_a();
}

float Circulo :: area(){
    return e_.area();
}
// Ejercicio 5

ostream& operator<<(ostream& os, Rectangulo r) {
    os << "Rect(" << r.alto() << ", " << r.ancho() << ")";
    return os;
}
ostream& operator<<(ostream& os, Elipse e) {
    os << "Elipse(" << e.r_a() << ", " << e.r_b() << ")";
    return os;
}

// ostream Elipse

// Ejercicio 6

ostream& operator<<(ostream& os, Circulo ci) {
    os << "Circ(" << ci.radio()  << ")";
    return os;
}
ostream& operator<<(ostream& os, Cuadrado cu) {
    os << "Cuad(" << cu.lado() << ")";
    return os;
}