#include <iostream>
#include <list>
using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();
    // Completar declaraciones funciones
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
  private:
    int mes_;
    int dia_;
    //Completar miembros internos
};

Fecha :: Fecha(int mes, int dia) : mes_(mes),dia_(dia){
};

int Fecha :: dia(){
    return dia_;
}

int Fecha :: mes(){
    return mes_;
}

void Fecha :: incrementar_dia(){
    if (dia_ + 1 > dias_en_mes(mes_) && mes_ != 11)
    {
        mes_ = mes_ + 1;
        dia_ = 1;
    }
    else if (dia_ + 1 > dias_en_mes(mes_) && mes_ == 11)
    {
        mes_ = 0;
        dia_ = 1;
    } else{
        dia_ = dia_ + 1;
    }
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}
#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    // Completar iguadad (ej 9)
    return igual_dia && igual_mes;
}
#endif

// Ejercicio 11, 12

// Clase Horario

class Horario{
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario g);
    bool operator<(Horario g);
private:
    uint hora_;
    uint min_;
};

Horario :: Horario(uint hora, uint min) : hora_(hora), min_(min){
};

uint Horario :: hora(){
    return hora_;
}

uint Horario :: min(){
    return min_;
}

bool Horario::operator==(Horario g) {
    bool igual_hora = this->hora() == g.hora();
    bool igual_min = this->min() == g.min();
    return igual_hora && igual_min;
}

bool Horario::operator<(Horario g) {
    bool res;
    if (this->hora() < g.hora()) { res = true; } else if (this->hora() > g.hora()) { res = false;} else if (this->min() < g.min() ) {res = true;} else {res = false;};
    return res;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}
// Ejercicio 13

class Recordatorio{
public:
    Recordatorio(Fecha, Horario, string);
    string r();
    Fecha f();
    Horario h();
    bool operator < (Recordatorio r);
private:
    Fecha f_;
    Horario h_;
    string r_;
};

Recordatorio :: Recordatorio(Fecha f, Horario h, string r) : f_(f), h_(h), r_(r){
};

Fecha Recordatorio :: f(){
    return f_;
}

Horario Recordatorio :: h(){
    return h_;
}

string Recordatorio :: r(){
    return r_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.r() << " @" << " " << r.f() << " " << r.h();
    return os;
}
// Clase Recordatorio


// Ejercicio 14

// Clase Agenda

class Agenda{
public:
    Agenda(Fecha);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();
private:
    Fecha hoy_;
    list<Recordatorio> r_;
};

Agenda :: Agenda(Fecha hoy) : hoy_(hoy){
};

void Agenda :: agregar_recordatorio(Recordatorio rec) {
    r_.push_back(rec);
}

void Agenda :: incrementar_dia() {
    hoy_.incrementar_dia();
}

Fecha Agenda :: hoy() {
    return hoy_;
}

bool Recordatorio::operator<(Recordatorio r) {
    bool menor_hora = this->h() < r.h();
    return menor_hora;
}

list<Recordatorio> Agenda :: recordatorios_de_hoy(){
    list<Recordatorio> res;
    for(Recordatorio x:r_)
    {
        if (x.f() == hoy_)
            res.push_back(x);
    }
    res.sort();
    return res;
}


ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << endl << "=====" << endl;
    for(Recordatorio x : a.recordatorios_de_hoy())
    {
        os << x << endl;
    }
    return os;
}