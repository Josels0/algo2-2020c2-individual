#include "Lista.h"

Lista::Lista() : first(nullptr), last(nullptr) {}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    int length = this->longitud();
    for(int i = 0; i < length; i++) {
        Nodo* temp = first->next;
        delete (first);
        first = temp;
    }
    last = first;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    int length = this->longitud();
    for(int i = 0; i < length; i++) {
        Nodo* temp = first->next;
        delete (first);
        first = temp;
    }
    last = first;
    for(int i = 0; i < aCopiar.longitud(); i++)
    {
        this->agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* temp = first;
    first = new Nodo(nullptr, elem, temp);
    if(temp == nullptr)
        last = first;
    else
        temp->before = first;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* temp = last;
    last = new Nodo(temp, elem, nullptr);
    if(temp == nullptr)
        first = last;
    else
        temp->next = last;
}

void Lista::eliminar(Nat i) {
    Nodo* toDelete = iesimoNodo(i);
    Nodo* before;
    Nodo* next;
    const int length = longitud();
    if(i > 0) {
        before = toDelete->before;
        before->next = nullptr;
    }
    else
        first = toDelete->next;
    if(i < length-1) {
        next = toDelete->next;
        next->before = nullptr;
    }
    else
        last = before;
    if(i > 0 && i < length-1) {
        before->next = next;
        next->before = before;
    }
    delete(toDelete);
}

int Lista::longitud() const {
    int length = 0;
    Nodo* temp = first;
    while(temp != nullptr){
        length++;
        temp = temp->next;
    }
    return length;
}

const int& Lista::iesimo(Nat i) const {
    int j = 0;
    Nodo* temp = first;
    while(j < i && i < this->longitud())
    {
        j++;
        temp = temp->next;
    }
    return temp->val;
}

int& Lista::iesimo(Nat i) {
    return iesimoNodo(i)->val;
}

void Lista::mostrar(ostream& o) {
    o << "[";
    for(int i = 0; i < longitud()-1; i++)
        o << iesimo(i) << ", ";
    o << last->val << "]";
}